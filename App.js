import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, SafeAreaView } from 'react-native';
import React from 'react';
import Colors from "./Constant/Colors";

import { NavigationContainer } from "@react-navigation/native";

import MainNavigator from "./Navigators/MainNavigator";
import { MaterialCommunityIcons } from "@expo/vector-icons";
// import { Provider } from "react-redux";
// import { store } from "./Store/store";
import { Provider } from 'react-redux';
import { Store } from '@reduxjs/toolkit';



export default function App() {
  return (
    <SafeAreaView style={styles.container}>
    <StatusBar style="light" backgroundColor={Colors.secondary}/>
    <Provider store={Store}>

    <NavigationContainer>
    <MainNavigator/>
      {/* <Login/> */}
      </NavigationContainer>
      </Provider>

  </SafeAreaView>
  
  );
}
const styles = StyleSheet.create({
  container: {
  flex: 1,
  backgroundColor: '#D2EFFF',
  },
  text: {
  fontSize: 25,
  fontWeight: '500',
  },
 secondary:{
  primary: "#FFCDB2",
  primary2: "#FFDFC9",
  secondary: "#FFB4A2",
  secondary2: "#FFCEBD",
  background: "#D2EFFF",
  backgroundWhite: "white",
  backgroundGrey: "#ffffff",
  text: "#333333",
  }
 });