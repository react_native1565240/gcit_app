// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

contract gcitCert {

    struct Certificate {
        string gcitCertId;
        string title;
        string date;
        address issuer;
        uint cgpa;
        string start;
        string end;
        uint duration;
    }
   
    mapping(string => Certificate) private certificates;
    mapping(address => string[]) private userCertificates;
   
    function addgcitCert(
        address _user,
        string memory _gcitCertId,
        string memory _title,
        string memory _date,
        address _issuer,
        uint _cgpa,
        string memory _start,
        string memory _end,
        uint _duration
    ) public {
        require(msg.sender == address(0x7129F1025d8118e75982d9A6cfED42B696D63210), "Only the authorized issuer can add certificates");
       
        Certificate memory newCertificate = Certificate({
            gcitCertId: _gcitCertId,
            title: _title,
                        date: _date,
            issuer: _issuer,
            cgpa: _cgpa,
            start: _start,
            end: _end,
            duration: _duration
        });
       
        certificates[_gcitCertId] = newCertificate;
        userCertificates[_user].push(_gcitCertId);
    }
   
    function verifygcitCert(string memory _gcitCertId) public view returns (bool) {
        return bytes(certificates[_gcitCertId].gcitCertId).length != 0;
    }
   
    function getListOfgcitCert(address _user) public view returns (Certificate[] memory) {
        string[] memory certificateIds = userCertificates[_user];
        Certificate[] memory userCertList = new Certificate[](certificateIds.length);
       
        for (uint i = 0; i < certificateIds.length; i++) {
            userCertList[i] = certificates[certificateIds[i]];
        }
       
        return userCertList;
    }
}

