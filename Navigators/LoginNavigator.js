import { createBottomTabNavigator }  from "@react-navigation/bottom-tabs";
import Colors from "../Constant/Colors";

import { MaterialCommunityIcons } from "@expo/vector-icons";
// import VerifyCert from "../Screens/VerifyCert";
// import Profile from "../Screens/Profile";
// import AddCertificate from "../Screens/AddCertificate";
// import Certificates from "../Screens/Certificates";
import VerifyCert from "../Screens/VerifyCert";
import Profile from "../Screens/Profile";
import AddCertificate from "../Screens/AddCertificate";
import Certificates from "../Screens/Certificate";

const Tab = createBottomTabNavigator();

function LoginNavigator() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerStyle: { backgroundColor: Colors.secondary },
        // tabBarShowLabel: false,
        tabBarActiveBackgroundColor: Colors.background,
        contentStyle: { backgroundColor: Colors.background },
      }}
    >
    <Tab.Screen
        name="Certificates"
        component={Certificates}
        options={{
          title: "List of Your Certificates",
          tabBarIcon: ({color, size}) => (
            <MaterialCommunityIcons
                name="certificate-outline"
                size={size}
                color={color}
            />
          )
        }}
    />

     <Tab.Screen
        name="Verify"
        component={VerifyCert}
        options={{
          title: "Verify Certificate",
          tabBarIcon: ({ size, color }) => (
            <MaterialCommunityIcons
              name="checkbox-marked-circle-outline"
              color={color}
              size={size}
            />
          ),
        }}
    />
    <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
            tabBarIcon: ({ size, color }) => (
                <MaterialCommunityIcons
                  name="account"
                  color={color}
                  size={size}
                />
              ),
            }}
        />

        <Tab.Screen
            name="AddCertificate"
            component={AddCertificate}
            options={{
            title: "Add Certificate",
            tabBarIcon: ({ color, size }) => (
            <MaterialCommunityIcons
                    name="certificate-outline"
                    size={size}
                    color={color}
                />
                ),
            }}
        />

        </Tab.Navigator>
      );
    }
    
export default LoginNavigator;
    
            

